import earthquake_assignment.earthquake_statistics
import earthquake_assignment.earthquake_stream

# Question 1 (Most Common Location for EQ)
es = earthquake_assignment.earthquake_statistics.EarthquakeStatistics()
es.iterate_file()
es.most_common_eq_location()

# Question 2 (EQ Histogram by Day)
es.get_hist_data()

# Question 3 (Average Magnitude by Location)
es.avg_mag_by_location()

# Question 4 (Mock Earthquake Stream using weighted average instead of sum)
e = earthquake_assignment.earthquake_stream.EarthquakeStream()
e.get_next_reading()
e.get_next_reading()
e.get_next_reading()
e.get_next_reading()
e.print_all_magnitudes()
