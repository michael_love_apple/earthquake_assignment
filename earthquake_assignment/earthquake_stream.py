import csv
from collections import defaultdict


class EarthquakeStream:

    def __init__(self):
        self.stream = csv.DictReader(open("earthquakes_1-6-18.csv"))
        self.freq = defaultdict(int)
        self.avg = defaultdict(float)
        return

    def get_next_reading(self):
        eq_reading = self.stream.__next__()
        self.add_weighted_avg(eq_reading['locationSource'], float(eq_reading['mag']))
        self.add_frequency(eq_reading['locationSource'])
        return

    def add_frequency(self, location):
        self.freq[location] += 1
        return

    def add_weighted_avg(self, location, magnitude):
        if location in self.avg:
            self.avg[location] = (self.avg[location] * (self.freq[location] /
                                 (self.freq[location] + 1))) + (magnitude *
                                 (1 / (self.freq[location] + 1)))
        else:
            self.avg[location] = magnitude
        return

    def print_all_magnitudes(self):
        for key in self.avg:
            print('{} {}'.format(key, self.avg[key]))
        return