import csv
from collections import Counter
from datetime import datetime
from pytz import timezone
from collections import defaultdict


class EarthquakeStatistics():

    def __init__(self):
        self.reader = csv.DictReader(open("earthquakes_1-6-18.csv"))
        self.locations = list()
        self.time = list()
        self.mag_sum = defaultdict(float)
        self.freq = defaultdict(int)
        return

    def iterate_file(self):
        for row in self.reader:
            self.locations.append(row['locationSource'])
            self.time.append(self.convert_time(row['time']))
            self.mag_sum[row['locationSource']] += float(row['mag'])
            self.freq[row['locationSource']] = self.freq[row['locationSource']] + 1

    def convert_time(self, date):
        date_formatted = datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.%fZ')
        date_formatted.replace(tzinfo=timezone('US/Pacific'))
        return date_formatted.strftime("%Y-%m-%d")

    def most_common_eq_location(self):
        return print(Counter(self.locations).most_common(1))

    def get_hist_data(self):
        return print(Counter(self.time))

    def avg_mag_by_location(self):
        for key in self.mag_sum:
            print('{} {}'.format(key, self.mag_sum[key] / self.freq[key]))
        return